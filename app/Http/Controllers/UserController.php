<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\usuario;
class UserController extends Controller
{
    //

    public function regUsuario(Request $request){
        $user = new usuario;
        $user->nombre      = $request->name;
        $user->apellido   = $request->apellido;
        $user->correo   = $request->email;
        $user->telefono   = $request->telefono;
        $user->save();
        return json_encode("Registro Exitoso");
    }
}
